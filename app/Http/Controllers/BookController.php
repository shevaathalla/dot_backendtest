<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{

    public function index(){
        $books = Book::all();
        if ($books) {
            return response()->json([
                'books' => $books
            ]);
        }
    }

    public function store(Request $request){
        $request->validate([
            'title' => ['required','string'],
            'synopsis' => ['required','string'],
            'page' => ['required','integer'],
            'author_id' => ['integer']
        ]);

        $author = Author::find($request->author_id);
        if (!$author) {
            return response()->json([
                'ERROR',
                'message' => 'Author tidak ada'
            ]);
        }

        $book = Book::create([
            'title' => request('title'),
            'synopsis' => request('synopsis'),
            'page' => request('page'),
            'author_id' => request('author_id'),
        ]);

        return response()->json(['message'=>'Buku berhasil ditambahkan','Book'=>$book],200);
    }

    public function show($id){
        $book = Book::where('id',$id)->first();        

        if ($book) {
            return response()->json([
                'title' => $book->title,
                'synopsis' => $book->synopsis,
                'page' => $book->page,
                'author' =>$book->author->name
            ],200);
        }else{
            return response()->json([
                'ERROR',
                'message'=>'Buku tidak ditemukan'
            ],400);
        }
    }
    

    public function search(Request $request){        
        $request->validate([
            'title' => ['string','required']
        ]);
        $book = Book::where('title','LIKE','%'.$request->title.'%')->first();
        
        if ($book) {
            return response()->json([
                'message' => 'Buku ditemukan',
                'title' => $book->title,
                'synopsis' => $book->synopsis,
                'page' => $book->page,
                'author' =>$book->author->name
            ],200);
        }else{
            return response()->json('Buku tidak ditemukan',400);
        }
    }

    public function update(Request $request, $id){
        $request->validate([
            'title' => ['required','string'],
            'synopsis' => ['required','string'],
            'page' => ['required','integer']
        ]);

        // dd($request->toArray());
        $book = Book::find($id);

        if ($book) {
            $book->title = $request->title;
            $book->synopsis = $request->synopsis;
            $book->page = $request->page;
            $book->save();
            return response()->json(['message' => 'Data book berhasil diubah',"book" => $book],200);
        }else{
            return response()->json(['message' => 'Data book tidak ditemukan'],404);
        }        
    }

    public function destroy($id){
        $book = Book::find($id);

        if ($book) {
            $book->delete();
            return response()->json(['message' => 'Data berhasil dihapus'],200);
        }else{
            return response()->json(['message' => 'Data book tidak ditemukan'],404);
        }
    }
}


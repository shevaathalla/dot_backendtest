<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(){
        $authors = Author::all();
        if ($authors) {
            return response()->json([
                'list' => $authors
            ]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['string','required']
        ]);

        $author = Author::create([
            'name' => request('name')
        ]);

        return response()->json(['Author baru ditambahkan', $author], 200);
    }

    public function update(Request $request, $id){

        $request->validate([
            'name' => ['string','required'],
        ]);

        // dd($request->toArray());
        $author = Author::find($id);

        if ($author) {
            $author->name = $request->name;
            $author->save();
            return response()->json(['message' => 'Data author berhasil diubah',"author" => $author],200);
        }else{
            return response()->json(['message' => 'Data author tidak ditemukan'],404);
        }        

    }

    public function show($id)
    {

        $author = Author::where('id', $id)->first();
        // dd($author->books);

        if ($author) {
            return response()->json([
                'name' => $author->name,
                'book' => $author->books,
            ]);
        }else{
            return response()->json([
                'message' => 'Author tidak ditemukan'
            ]);
        }
    }

    public function destroy($id)
    {        
        $author = Author::find($id);
        if ($author) {
            $author->delete();
            return response()->json(['message' => 'Data berhasil dihapus'],200);
        }else{
            return response()->json(['message' => 'Data author tidak ditemukan'],404);
        }
    }

}

# DOT Backend Test Internship

## Penjelasan Project
Project ini adalah project API daftar buku, di dalam project ini terdapat autentikasi, CRUD pembuat buku (Model Author) dan CRUD buku (Model Book), project ini dibuat dengan framework backend Laravel 7. Untuk bagian autentikasi project ini menggunakan salah satu package di laravel yaitu laravel passport

## ERD Database
Gambar dari desain tabel yang dibuat
![ERD](public/img/erd.png)

## Dokumentasi API
Model Auth: https://documenter.getpostman.com/view/15292396/TzY4eunR
Model Author: https://documenter.getpostman.com/view/15292396/TzY4eunS
Model Book: https://documenter.getpostman.com/view/15292396/TzY4eunT